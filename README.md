> Tasks for extracting/compiling angular-gettext strings.

copy of this [`project`](https://github.com/rubenv/grunt-angular-gettext), except it's support 


```

<span>{{ 'some_txt' | translate:"blahblah" }}</span> 

```

where 
```

blahblah
```
 it's a context of 
```
#!html

some_txt
```
The author of the original packet does not want to add this functionality. This Pack for my project and I do not support it.

Used in combination with [`angular-gettext`](https://github.com/rubenv/angular-gettext).

Original Package [`angular-gettext`](https://travis-ci.org/rubenv/grunt-angular-gettext.png?branch=master)